%%%%
amulet

A piece of jewellery.
%%%%
book

A book of magic spells. Beware, for some of the more powerful grimoires
are not to be toyed with.
%%%%
deck of cards

A deck of powerful magical cards painted in the ichor of demons. They may have any number of helpful or dangerous effects when drawn.
%%%%
potion

A small bottle of liquid.
%%%%
ring

A piece of jewellery.
%%%%
rod

A stick imbued with magical properties.
%%%%
scroll

A scroll of paper covered in magical writing.
%%%%
magical staff

A stick imbued with magical properties.
%%%%
wand

A stick. Maybe it's magical.
%%%%
